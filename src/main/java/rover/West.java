package rover;

public class West implements Directions {
    @Override
    public Directions turnRight() {
        return new North();
    }

    @Override
    public Directions turnLeft() {
        return new South();
    }

    @Override
    public Position move(Position position) {
        position.x = position.left();
        return position;
    }

    @Override
    public String toString() {
        return "W";
    }
}
