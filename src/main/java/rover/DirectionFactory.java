package rover;

public class DirectionFactory {
	// This class is untested

    public Directions createDirection(String input){

    	/* I don't like returning East by default here, because the input is too permissive...
    	what if the input was "Hello", createDirection would still return East. Is that OK?

    	How would you handle the exceptional behaviour mentioned above? 
    	Could the design been improved if it was TDDed? :)
    	*/

        if(input.equals("N"))
            return new North();
        if(input.equals("S"))
            return new South();
        if(input.equals("W"))
            return new West();
        if(input.equals("E"))
            return new East();

        return null;
    }
}
