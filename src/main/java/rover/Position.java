package rover;

class Position {
    // This class is untested

    int x;
    int y;

    Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    int up(){
        return this.y += 1;
    }

    int down(){
        return this.y -= 1;
    }

    int left(){
        return this.x -= 1;
    }

    int right(){
        return this.x += 1;
    }
}
