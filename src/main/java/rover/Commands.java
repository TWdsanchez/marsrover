package rover;

public enum Commands {
    M, R, L;

    public static void validateCommands(String input, String[] commandArray) {
        for (String command : commandArray) {
            try{
                Commands.valueOf(command);
            }catch (Exception e){
                throw new IllegalArgumentException("Invalid command sequence: " + input);
            }
        }
    }

    public static String[] convertInputIntoCommands(String input) {
        String[] commandArray = input.split("(?!^)");
        Commands.validateCommands(input, commandArray);
        return commandArray;
    }

}
