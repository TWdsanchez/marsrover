package rover;

public class MarsRover {

    private Position position;
    private Directions currentDirection;

    MarsRover(int startingX, int startingY, String direction) {
        position = new Position(startingX, startingY);
        DirectionFactory directionFactory = new DirectionFactory();
        this.currentDirection = directionFactory.createDirection(direction);
    }

    public String run(String input) {
        String[] commands = Commands.convertInputIntoCommands(input);

        for (String command : commands) {
            runCommand(command);
        }

        return asString();
    }

    private void runCommand(String command) {
        if (Commands.valueOf(command) == Commands.M) {
            move();
        } else if (Commands.valueOf(command) == Commands.R) {
            turnRight();
        } else if (Commands.valueOf(command) == Commands.L) {
            turnLeft();
        }
    }

    private void move() {
        position = currentDirection.move(this.position);
    }

    private String asString() {
        return position.x + " " + position.y + " " +
                currentDirection.toString();
    }

    private void turnLeft() {
        currentDirection = currentDirection.turnLeft();
    }

    private void turnRight() {
        currentDirection = currentDirection.turnRight();
    }

}
