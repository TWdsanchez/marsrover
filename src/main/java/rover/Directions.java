package rover;

public interface Directions{
	// Is there a need for an abstract class here? There aren't any implementations.
	// None of the implementations (or children of) Directions are tested! :(
    Directions turnRight();
    Directions turnLeft();
    Position move(Position position);
}
