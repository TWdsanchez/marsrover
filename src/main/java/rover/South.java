package rover;

public class South implements Directions {
    @Override
    public Directions turnRight() {
        return new West();
    }

    @Override
    public Directions turnLeft() {
        return new East();
    }

    @Override
    public Position move(Position position) {
        position.y = position.down();
        return position;
    }

    @Override
    public String toString() {
        return "S";
    }
}
