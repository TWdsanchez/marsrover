package rover;

public class North implements Directions{
    @Override
    public Directions turnRight() {
        return new East();
    }

    @Override
    public Directions turnLeft() {
        return new West();
    }

    @Override
    public Position move(Position position) {
        position.y = position.up();
        return position;
    }

    @Override
    public String toString() {
        return "N";
    }
}
