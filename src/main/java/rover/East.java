package rover;

public class East implements Directions {
    @Override
    public Directions turnRight() {
        return new South();
    }

    @Override
    public Directions turnLeft() {
        return new North();
    }

    @Override
    public Position move(Position position) {
        position.x = position.right();
        return position;
    }

    @Override
    public String toString() {
        return "E";
    }
}
