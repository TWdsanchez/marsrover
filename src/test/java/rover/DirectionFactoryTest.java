package rover;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DirectionFactoryTest {
    private DirectionFactory directionFactory = new DirectionFactory();

    @Test
    void createNorth() {
        assertEquals(North.class,
                directionFactory.createDirection("N").getClass());
    }

    @Test
    void createSouth() {
        assertEquals(South.class,
                directionFactory.createDirection("S").getClass());
    }

    @Test
    void createEast() {
        assertEquals(East.class,
                directionFactory.createDirection("E").getClass());
    }

    @Test
    void createWest() {
        assertEquals(West.class,
                directionFactory.createDirection("W").getClass());
    }

    @Test
    void createWrongDirection() {
        assertEquals(null,
                directionFactory.createDirection("abc"));
    }
}
