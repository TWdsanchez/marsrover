package rover;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class DirectionsTest {

    private East east;
    private North north;
    private South south;
    private West west;
    private Position position;

    @BeforeEach
    void setUp() {
        east = new East();
        north = new North();
        south = new South();
        west = new West();
        position = new Position(0,0);
    }

    @Test
    void eastTurnLeftShouldBeNorth() {
        assertEquals(east.turnLeft().getClass(), North.class);
    }

    @Test
    void eastTurnRightShouldBeSouth() {
        assertEquals(east.turnRight().getClass(), South.class);
    }

    @Test
    void eastToStringShouldBeE() {
        assertEquals(east.toString(), "E");
    }

    @Test
    void eastMoveShouldBeRight() {
        position = east.move(position);
        assertEquals(position.x, 1);
        assertEquals(position.y, 0);
    }

    @Test
    void northTurnLeftShouldBeWest() {
        assertEquals(north.turnLeft().getClass(), West.class);
    }

    @Test
    void northTurnRightShouldBeEast() {
        assertEquals(north.turnRight().getClass(), East.class);
    }

    @Test
    void northToStringShouldBeN() {
        assertEquals(north.toString(), "N");
    }

    @Test
    void northMoveShouldBeUp() {
        position = north.move(position);
        assertEquals(position.x, 0);
        assertEquals(position.y, 1);
    }


    @Test
    void southTurnLeftShouldBeEast() {
        assertEquals(south.turnLeft().getClass(), East.class);
    }

    @Test
    void southTurnRightShouldBeWest() {
        assertEquals(south.turnRight().getClass(), West.class);
    }

    @Test
    void southToStringShouldBeS() {
        assertEquals(south.toString(), "S");
    }

    @Test
    void southMoveShouldBeDown() {
        position = south.move(position);
        assertEquals(position.x, 0);
        assertEquals(position.y, -1);
    }

    @Test
    void westTurnLeftShouldBeSouth() {
        assertEquals(west.turnLeft().getClass(), South.class);
    }

    @Test
    void westTurnRightShouldBeNorth() {
        assertEquals(west.turnRight().getClass(), North.class);
    }

    @Test
    void westToStringShouldBeW() {
        assertEquals(west.toString(), "W");
    }

    @Test
    void westShouldMoveLeft() {
        position = west.move(position);
        assertEquals(position.x, -1);
        assertEquals(position.y, 0);
    }


}
